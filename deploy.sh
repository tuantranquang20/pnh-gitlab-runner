echo "Deploying....."
rm -rf $HOME/pnh-gitlab-runner
mkdir -p $HOME/pnh-gitlab-runner
cd $HOME/pnh-gitlab-runner
# git clone https://tuantranquang20:$DEPLOY_TOKEN@gitlab.com/tuantranquang20/pnh-gitlab-runner
git clone https://gitlab.com/tuantranquang20/pnh-gitlab-runner.git .
docker-compose down
docker-compose up -d
echo "deployed"